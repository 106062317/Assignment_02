function Enemy(config) {
    this.init = function() {
      this.enemys = game.add.group();
      this.enemys.enableBody = true;
      this.enemys.createMultiple(20, config.selfPic);
      this.enemys.setAll('outOfBoundsKill', true);
      this.enemys.setAll('checkWorldBounds', true);

      this.enemyBullets = game.add.group();
      this.enemyBullets.enableBody = true;
      this.enemyBullets.createMultiple(100, config.bulletPic);
      this.enemyBullets.setAll('outOfBoundsKill', true);
      this.enemyBullets.setAll('checkWorldBounds', true);
      this.emitter = game.add.emitter(0, 0, 150); 
      this.emitter.makeParticles('pixel'); 
      this.emitter.setYSpeed(-50, 50); 
      this.emitter.setXSpeed(-50, 50); 
      this.emitter.setScale(2, 0, 2, 0, 800); 
      this.emitter.gravity = 0;
      
      this.maxWidth = game.width - game.cache.getImage(config.selfPic).width;
      game.time.events.loop(config.selfTimeInterval, this.generateEnemy, this); 
      if(config.selfPic=="enemy2"){
        game.time.events.add(120000, this.generateTeam, this);
      }    
      if(config.selfPic=="enemy3"){
        game.time.events.add(63000, function() {
          game.time.events.loop(config.selfTimeInterval, this.generateEnemy, this);}, this);
      }     

      this.explosions = game.add.group();
      this.explosions.createMultiple(20, config.explodePic);
      this.explosions.forEach(function(explosion) {
        explosion.animations.add(config.explodePic);
      }, this);
    },
    this.generateEnemy = function() {
      var e = this.enemys.getFirstExists(false);
      if(e && game.boss!=1) {
        e.reset(game.rnd.integerInRange(0, this.maxWidth), -game.cache.getImage(config.selfPic).height);
        e.life = config.life;
        e.body.velocity.y = config.velocity;
      }
    }
    this.generateTeam = function() {
      this.d = Math.floor((Math.random() * 100) + 70);
      if(game.boss!=1) {
        game.time.events.add(15000, this.generateTeam, this);
        this.generateTeamMem();
        game.time.events.add(1000, this.generateTeamMem, this);
        game.time.events.add(2000, this.generateTeamMem, this);
        game.time.events.add(3000, this.generateTeamMem, this);
        game.time.events.add(4000, this.generateTeamMem, this);
        game.time.events.add(5000, this.generateTeamMem, this);
      }
    }
    this.generateTeamMem = function() {
      var e;
      e = this.enemys.getFirstExists(false);
      if(e && game.boss!=1) {
        e.reset(180-this.d, -game.cache.getImage(config.selfPic).height);
        e.life = config.life;
        e.body.velocity.y = config.velocity;
      }
      e = this.enemys.getFirstExists(false);
      if(e && game.boss!=1) {
        e.reset(120+this.d, -game.cache.getImage(config.selfPic).height);
        e.life = config.life;
        e.body.velocity.y = config.velocity;
      }
    }
    this.enemyFire = function() {
      this.enemys.forEachExists(function(enemy) {
        var bullet = this.enemyBullets.getFirstExists(false);
        if(bullet) {
          if(game.time.now > (enemy.bulletTime || 0)) {
            bullet.reset(enemy.x + game.cache.getImage(config.selfPic).width/2 -2, enemy.y + game.cache.getImage(config.selfPic).height);
            bullet.body.velocity.y = config.bulletVelocity;
            enemy.bulletTime = game.time.now + config.bulletTimeInterval;
          }
        }
          }, this);
    };
    this.hitEnemy = function(myBullet, enemy) {
      myBullet.kill();
      this.emitter.x = myBullet.x+4; 
      this.emitter.y = myBullet.y+10; 
      this.emitter.start(true, 800, null, 15); 
      enemy.life--;
      if(enemy.life <= 0) {
        game.crash.play();
        enemy.kill();
        var explosion = this.explosions.getFirstExists(false);
        var x = game.cache.getImage(config.selfPic).width/2 - game.cache.getImage(config.explodePic).width/6;
        var y = game.cache.getImage(config.selfPic).height/2 - game.cache.getImage(config.explodePic).height/6;
        explosion.reset(enemy.body.x + x, enemy.body.y + y);
        explosion.play(config.explodePic, 10, false, true);
        game.global.score += config.score;
        config.game.updateText();
      }
    };
    this.hitall = function() {
      this.enemys.forEachExists(function(enemy) {
        enemy.kill();
        var explosion = this.explosions.getFirstExists(false);
        var x = game.cache.getImage(config.selfPic).width/2 - game.cache.getImage(config.explodePic).width/6;
        var y = game.cache.getImage(config.selfPic).height/2 - game.cache.getImage(config.explodePic).height/6;
        explosion.reset(enemy.body.x + x, enemy.body.y + y);
        explosion.play(config.explodePic, 10, false, true);
        game.global.score += config.score;
        config.game.updateText();
      }, this);
    };
  }

// Initialize Phaser 
var game = new Phaser.Game(360, 500, Phaser.CANVAS, 'canvas'); 

// Define our global variable 
game.global = { score: 0, level: 1 , name: ""}; 
// Add all the states 
game.state.add('boot', bootState); 
game.state.add('load', loadState); 
game.state.add('menu', menuState); 
game.state.add('play', playState); 
game.state.add('play2', play2State); 
game.state.add('win', winState); 
game.state.add('lose', loseState); 
// Start the 'boot' state 
game.state.start('boot');

var menum = document.getElementById("mainmenu");
var menur = document.getElementById("rankmenu");
var menus = document.getElementById("setmenu");
var menui = document.getElementById("intromenu");
var menup = document.getElementById("pausemenu");
var menure = document.getElementById("restartmenu");
var music = document.getElementById("music");
var sound = document.getElementById("sound");
var vm = document.getElementById("vm");
var vs = document.getElementById("vs");
vm.innerHTML = music.value;
vs.innerHTML = sound.value;
music.oninput = function() {
  vm.innerHTML = music.value;
}
sound.oninput = function() {
  vs.innerHTML = sound.value;
}
function start() {
  menum.style.display = "none";
  menur.style.display = "none";
  menus.style.display = "none";
  menui.style.display = "none";
  getName();
  var m0 = document.getElementById("1p");
  var m1 = document.getElementById("2p");
  if(m0.selected) game.state.start('play'); 
  else if(m1.selected) game.state.start('play2'); 
} 
function getName() {
  var name = prompt("      君の名は (<10)", "");
  if (name == null || name == "") {
    getName();
  } else {
    game.global.name = name;
  }
}
function set() {
  menus.style.display = "block";
} 
function closeset() {
  menus.style.display = "none";
} 
function rank() {
  menur.style.display = "block";
} 
function closerank() {
  menur.style.display = "none";
} 
function intro() {
  menui.style.display = "block";
} 
function closeintro() {
  menui.style.display = "none";
} 
function cont() {
  menup.style.display = "none";
  game.paused = false;
} 
function back() {
  menup.style.display = "none";
  game.paused = false;
  game.bgm.stop();
  game.state.start('menu');
} 
function backm() {
  menure.style.display = "none";
  game.state.start('menu');
} 