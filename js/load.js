var loadState = {
  preload: function() {
    var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' }); 
    loadingLabel.anchor.setTo(0.5, 0.5);
    
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5); 
    game.load.setPreloadSprite(progressBar);
    
    game.load.image('background', 'assets/bg.png');
    game.load.image('lose', 'assets/lose.png');
    game.load.image('gg', 'assets/gg.png');
    game.load.spritesheet('myplane', 'assets/plane.png', 50, 50, 4);
    game.load.spritesheet('plane1', 'assets/plane1.png', 50, 50, 4);
    game.load.spritesheet('plane2', 'assets/plane2.png', 50, 50, 4);
    game.load.image('mybullet', 'assets/mybullet.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('enemy1', 'assets/enemy1.png');
    game.load.image('enemy2', 'assets/enemy2.png');
    game.load.image('enemy3', 'assets/enemy3.png');
    game.load.image('alien', 'assets/alien.png');
    game.load.image('boss', 'assets/boss.png');
    game.load.spritesheet('explode1', 'assets/explode1.png', 30, 30, 3);
    game.load.spritesheet('explode2', 'assets/explode2.png', 35, 35, 3);
    game.load.spritesheet('explode3', 'assets/explode3.png', 50, 50, 3);
    game.load.spritesheet('myexplode', 'assets/myexplode.png', 50, 50, 3);
    game.load.image('bulup', 'assets/item1.png');
    game.load.image('help', 'assets/item2.png');
    game.load.image('pixel', 'assets/pixel.png'); 
    game.load.image('mypixel', 'assets/mypixel.png');
    game.load.image('pause', 'assets/pause.png');
    game.load.audio('bgm',  ['assets/bgm.ogg', 'assets/bgm.mp3']);
    game.load.audio('shoot', ['assets/shoot.ogg', 'assets/shoot.mp3']);
    game.load.audio('get', ['assets/get.ogg', 'assets/get.mp3']);
    game.load.audio('crash', ['assets/crash.ogg', 'assets/crash.mp3']);
    game.load.audio('dead', ['assets/dead.ogg', 'assets/dead.mp3']);
    game.load.audio('win', ['assets/win.ogg', 'assets/win.mp3']);
    var db = firebase.database();
    db.ref("1p").once('value').then(function (snapshot) {
      var data = snapshot.val();
      document.getElementById('r11').innerHTML = "1." + data.rank1.name + ":" + data.rank1.score;
      document.getElementById('r21').innerHTML = "2." + data.rank2.name + ":" + data.rank2.score;
      document.getElementById('r31').innerHTML = "3." + data.rank3.name + ":" + data.rank3.score;
      document.getElementById('r41').innerHTML = "4." + data.rank4.name + ":" + data.rank4.score;
      document.getElementById('r51').innerHTML = "5." + data.rank5.name + ":" + data.rank5.score;
      db.ref("2p").once('value').then(function (snapshot) {
        var data = snapshot.val();
        document.getElementById('r12').innerHTML = "1." + data.rank1.name + ":" + data.rank1.score;
        document.getElementById('r22').innerHTML = "2." + data.rank2.name + ":" + data.rank2.score;
        document.getElementById('r32').innerHTML = "3." + data.rank3.name + ":" + data.rank3.score;
        document.getElementById('r42').innerHTML = "4." + data.rank4.name + ":" + data.rank4.score;
        document.getElementById('r52').innerHTML = "5." + data.rank5.name + ":" + data.rank5.score;
      });  
    });  
  }, 
  create: function() { 
    game.state.start('menu'); 
  } 
}; 
