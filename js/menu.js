var menuState = { 
  create: function() {
    var mainmenu = document.getElementById("mainmenu");
    mainmenu.style.display = "block";
    // Add a background image 
    var bg = game.add.tileSprite(0, 0, game.width, game.height, 'background');
    // Display the name of the game 
    var nameLabel = game.add.text(game.width/2, -50, 'Assignment 2', { font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5); 
    game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();

    this.myplane = game.add.sprite(game.width/2, game.height/2-50, 'myplane');
    this.myplane.anchor.setTo(0.5, 0.5);
    this.myplane.scale.setTo(2, 2);
    this.myplane.animations.add('fly');
    this.myplane.animations.play('fly', 12, true);
  }
}; 