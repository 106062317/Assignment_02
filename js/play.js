var playState = { 
  create: function() {
    game.global.score = 0;
    game.global.level = 1;
    game.boss = 0;
    this.helping = 0;
    this.conshoot = 1;
    this.gena = 1;

    this.bg = game.add.tileSprite(0, 0, game.width, game.height, 'background');
    this.bg.autoScroll(0, 20);
    
    this.myplane = game.add.sprite(game.width/2, game.height/2-50, 'myplane');
    this.myplane.anchor.setTo(0.5, 0.5);
    this.myplane.scale.setTo(2, 2);
    this.myplane.animations.add('fly');
    this.myplane.animations.play('fly', 12, true);
    game.physics.arcade.enable(this.myplane);
    this.myplane.body.collideWorldBounds = true;
    this.myplane.bulletLv = 2;
    this.myplane.health = 5;
    
    game.add.tween(this.myplane).to({y: game.height - 40}, 1000, Phaser.Easing.Sinusoidal.InOut, true);
    game.add.tween(this.myplane.scale).to({x: 1, y: 1}, 1000, Phaser.Easing.Sinusoidal.InOut, true);

    var voicem = document.getElementById("music").value/100;
    var voices = document.getElementById("sound").value/100;
    game.bgm = game.add.audio('bgm', voicem, true);
    game.bgm.play();
    this.shoot = game.add.audio('shoot', voices, false);
    game.crash = game.add.audio('crash', voices, false);
    this.get = game.add.audio('get', voices, false);
    this.deads = game.add.audio('dead', voices, false);
    this.wins = game.add.audio('win', voices, false);
    
    this.mybullets = game.add.group();
    this.mybullets.enableBody = true;
    this.mybullets.createMultiple(50, 'mybullet');
    this.mybullets.setAll('outOfBoundsKill', true);
    this.mybullets.setAll('checkWorldBounds', true);
    this.bulletTime = 0;

    this.emitters = game.add.emitter(0, 0, 150); 
    this.emitters.makeParticles('pixel'); 
    this.emitters.setYSpeed(-50, 50); 
    this.emitters.setXSpeed(-50, 50); 
    this.emitters.setScale(2, 0, 2, 0, 800); 
    this.emitters.gravity = 0;
    this.myemitter = game.add.emitter(0, 0, 150); 
    this.myemitter.makeParticles('mypixel'); 
    this.myemitter.setYSpeed(-50, 50); 
    this.myemitter.setXSpeed(-50, 50); 
    this.myemitter.setScale(2, 0, 2, 0, 800); 
    this.myemitter.gravity = 0;

    this.skillnum = 3;
    this.cursor = game.input.keyboard.createCursorKeys();

    this.bullets1 = game.add.group();
    this.bullets1.enableBody = true;
    this.bullets1.createMultiple(50, 'mybullet');
    this.bullets1.setAll('outOfBoundsKill', true);
    this.bullets1.setAll('checkWorldBounds', true);
    this.bullets2 = game.add.group();
    this.bullets2.enableBody = true;
    this.bullets2.createMultiple(50, 'mybullet');
    this.bullets2.setAll('outOfBoundsKill', true);
    this.bullets2.setAll('checkWorldBounds', true);
    
    this.bulups = game.add.group();
    this.bulups.enableBody = true;
    this.bulups.createMultiple(1, 'bulup');
    this.bulups.setAll('outOfBoundsKill', true);
    this.bulups.setAll('checkWorldBounds', true);
    this.bulupMaxWidth = game.width - game.cache.getImage('bulup').width;

    this.help = game.add.group();
    this.help.enableBody = true;
    this.help.createMultiple(1, 'help');
    this.help.setAll('outOfBoundsKill', true);
    this.help.setAll('checkWorldBounds', true);
    this.helpMaxWidth = game.width - game.cache.getImage('help').width;

    this.aliens = game.add.group();
    this.aliens.enableBody = true;
    this.aliens.createMultiple(50, 'alien');
    this.aliens.setAll('outOfBoundsKill', true);
    this.aliens.setAll('checkWorldBounds', true);
    this.aliens.forEach(function(alien) {
      alien.anchor.setTo(0.5, 0.5);
    }, this);
    this.alienTime = 0;
    
    game.time.events.loop(17000, this.generatebulup, this);
    game.time.events.loop(23000, this.generatehelp, this);
    game.time.events.loop(60000, this.updatelevel, this);
    game.time.events.add(180000, this.bossfight, this);
    
    this.enemyTeam = {
      enemy1: {
        game: this,
        selfPic: 'enemy1',
        bulletPic: 'bullet',
        explodePic: 'explode1',
        life: 2,
        velocity: 50,
        bulletVelocity: 200,
        selfTimeInterval: 2000,
        bulletTimeInterval: 1000,
        score: 10
      },
      enemy2: {
        game: this,
        selfPic: 'enemy2',
        bulletPic: 'bullet',
        explodePic: 'explode2',
        life: 5,
        velocity: 40,
        bulletVelocity: 250,
        selfTimeInterval: 3000,
        bulletTimeInterval: 1200,
        score: 20
      },
      enemy3: {
        game: this,
        selfPic: 'enemy3',
        bulletPic: 'bullet',
        explodePic: 'explode3',
        life: 10,
        velocity: 30,
        bulletVelocity: 300,
        selfTimeInterval: 6000,
        bulletTimeInterval: 1500,
        score: 50
      }
    }
    this.enemy1 = new Enemy(this.enemyTeam.enemy1);
    this.enemy1.init();
    this.enemy2 = new Enemy(this.enemyTeam.enemy2);
    this.enemy2.init();
    this.enemy3 = new Enemy(this.enemyTeam.enemy3);
    this.enemy3.init();

    this.boss = game.add.sprite(180, -70, 'boss');
    this.boss.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(this.boss);

    this.lifeLabel = game.add.text(200, 10,'Health: 5', { font: '25px Arial', fill: '#ffffff' }); 
    this.levelLabel = game.add.text(310, 10,'1/3', { font: '25px Arial', fill: '#ffffff' }); 
    this.scoreLabel = game.add.text(200, 40, 'Score: 0', { font: '25px Arial', fill: '#ffffff' }); 
    this.hpLabel = game.add.text(180, -25, '100', { font: '25px Arial', fill: '#ff0000' });
    this.hpLabel.anchor.setTo(0.5, 0); 
    this.skillbutton = game.add.text(10, 470, 'Skill: 3', { font: '25px Arial', fill: '#ffffff' });
    this.skillbutton.inputEnabled = true;
    this.skillbutton.events.onInputDown.add(this.onSkillClick, this);
    this.pausebutton = game.add.button(10, 10, 'pause', this.onPauseClick, this);
  }, 
  update: function() {
    this.movePlayer();
    this.myFireBullet();
    this.enemy1.enemyFire();
    if(game.global.level < 3) this.enemy2.enemyFire();
    this.enemy3.enemyFire();
    if(game.boss){
      this.moveAliens();
      game.physics.arcade.overlap(this.aliens, this.myplane, this.hitMyplane, null, this);
      game.physics.arcade.overlap(this.boss, this.myplane, this.crashMyplane, null, this);
      game.physics.arcade.overlap(this.mybullets, this.aliens, this.hitAlien, null, this);
      game.physics.arcade.overlap(this.mybullets, this.boss, this.hitBoss, null, this);
    }
    game.physics.arcade.overlap(this.mybullets, this.enemy1.enemys, this.enemy1.hitEnemy, null, this.enemy1);
    game.physics.arcade.overlap(this.mybullets, this.enemy2.enemys, this.enemy2.hitEnemy, null, this.enemy2);
    game.physics.arcade.overlap(this.mybullets, this.enemy3.enemys, this.enemy3.hitEnemy, null, this.enemy3);
    game.physics.arcade.overlap(this.bullets1, this.enemy1.enemys, this.enemy1.hitEnemy, null, this.enemy1);
    game.physics.arcade.overlap(this.bullets1, this.enemy2.enemys, this.enemy2.hitEnemy, null, this.enemy2);
    game.physics.arcade.overlap(this.bullets1, this.enemy3.enemys, this.enemy3.hitEnemy, null, this.enemy3);
    game.physics.arcade.overlap(this.bullets2, this.enemy1.enemys, this.enemy1.hitEnemy, null, this.enemy1);
    game.physics.arcade.overlap(this.bullets2, this.enemy2.enemys, this.enemy2.hitEnemy, null, this.enemy2);
    game.physics.arcade.overlap(this.bullets2, this.enemy3.enemys, this.enemy3.hitEnemy, null, this.enemy3);
    game.physics.arcade.overlap(this.enemy1.enemyBullets, this.myplane, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemyBullets, this.myplane, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemyBullets, this.myplane, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy1.enemys, this.myplane, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemys, this.myplane, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemys, this.myplane, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.bulups, this.myplane, this.getbulup, null, this);
    game.physics.arcade.overlap(this.help, this.myplane, this.gethelp, null, this);
  },
  movePlayer: function() {
    if (this.cursor.left.isDown) { 
      this.myplane.body.velocity.x = -200;
    }else if (this.cursor.right.isDown) { 
      this.myplane.body.velocity.x = 200;
    }else {
      this.myplane.body.velocity.x = 0; 
    }    

    if (this.cursor.up.isDown) { 
      this.myplane.body.velocity.y = -200;
    }else if (this.cursor.down.isDown) { 
      this.myplane.body.velocity.y = 200;
    }else {
      this.myplane.body.velocity.y = 0; 
    }      
    if(this.helping){
      this.helper1.x = this.myplane.x - 40;
      this.helper1.y = this.myplane.y;
      this.helper2.x = this.myplane.x + 40;
      this.helper2.y = this.myplane.y;
    }
  },
  moveAliens: function() {
    this.aliens.forEachExists(function(alien) {
      if(alien.x > this.myplane.x) {
        alien.body.velocity.x =  -20;
      }else if(alien.x < this.myplane.x) {
        alien.body.velocity.x = 20;
      }else{
        alien.body.velocity.x = 0;
      }
    }, this);
  },
  myFireBullet: function() {
    if(this.myplane.alive && game.time.now > this.bulletTime && this.conshoot) {
      try {
        this.shoot.play();
      } catch(e) {}
      var bullet;
      bullet = this.mybullets.getFirstExists(false);
      if(bullet) {
        bullet.reset(this.myplane.x -4, this.myplane.y -45);
        bullet.body.velocity.y = -400;
        this.bulletTime = game.time.now + 200;
      }
      if(this.myplane.bulletLv >= 2) {
        bullet = this.mybullets.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.myplane.x -4, this.myplane.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -40;
          this.bulletTime = game.time.now + 200;
        }
        bullet = this.mybullets.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.myplane.x -4, this.myplane.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 40;
          this.bulletTime = game.time.now + 200;
        }
      }
      if(this.myplane.bulletLv >= 3) {
        bullet = this.mybullets.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.myplane.x -4, this.myplane.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -80;
          this.bulletTime = game.time.now + 200;
        }
        bullet = this.mybullets.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.myplane.x -4, this.myplane.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 80;
          this.bulletTime = game.time.now + 200;
        }
      }
      if(this.helping){
        var bullet1;
        bullet1 = this.bullets1.getFirstExists(false);
        bullet1.reset(this.helper1.x -4, this.helper1.y -25);
        bullet1.body.velocity.y = -400;
        bullet1.body.velocity.x = -40*this.myplane.bulletLv;
        var bullet2;
        bullet2 = this.bullets1.getFirstExists(false);
        bullet2.reset(this.helper2.x -4, this.helper2.y -25);
        bullet2.body.velocity.y = -400;
        bullet2.body.velocity.x = 40*this.myplane.bulletLv;
      }
    }
  },
  hitMyplane: function(myplane, bullet) {
    bullet.kill();
    this.myemitter.x = bullet.x+4; 
    this.myemitter.y = bullet.y+10; 
    this.myemitter.start(true, 800, null, 15); 
    if(myplane.health > 1) {
      myplane.health --;
      if(myplane.bulletLv > 1) myplane.bulletLv--;
      this.lifeLabel.setText('Health: ' + this.myplane.health);
    } else {
      this.lifeLabel.setText('Health: 0');
      myplane.kill();
      this.dead();
    }
  },
  hitAlien: function(alien, bullet) {
    bullet.kill();
    this.emitters.x = bullet.x+4; 
    this.emitters.y = bullet.y+10; 
    this.emitters.start(true, 800, null, 15);
    alien.kill();
    game.global.score += 5;
    this.updateText();
  },
  hitBoss: function(boss, bullet) {
    bullet.kill();
    if(this.bosshp > 1) {
      this.bosshp --;
      this.hpLabel.setText(this.bosshp);
      this.emitters.x = bullet.x+4; 
      this.emitters.y = bullet.y+10; 
      this.emitters.start(true, 800, null, 15);
    } else {
      this.conshoot = 0;
      this.hpLabel.setText(0);
      this.explode = game.add.sprite(this.boss.x, this.boss.y, 'explode3');
      this.explode.anchor.setTo(0.5, 0.5);
      this.explode.scale.setTo(3, 3);
      this.explode.animations.add('explode3');
      this.explode.animations.play('explode3', 10, false);
      this.aliens.forEachExists(function(alien) {
        alien.kill();
      }, this);
      this.gena = 0;
      game.global.score += 5000;
      game.crash.play();
      this.win();
    }
  },
  crashMyplane: function(myplane, enemy) {
    this.lifeLabel.setText('Health: 0');
    this.myplane.kill();
    this.dead();
  },
  generatebulup: function() {
    var bulup = this.bulups.getFirstExists(false);
    if(bulup && game.boss!=1) {
      bulup.reset(game.rnd.integerInRange(0, this.bulupMaxWidth), -game.cache.getImage('bulup').height);
      bulup.body.velocity.y = 100;
    }
  },
  generatehelp: function() {
    var help = this.help.getFirstExists(false);
    if(help && game.boss!=1) {
      help.reset(game.rnd.integerInRange(0, this.helpMaxWidth), -game.cache.getImage('help').height);
      help.body.velocity.y = 100;
    }
  },
  getbulup: function(myplane, bulup) {
    bulup.kill();
    if(myplane.bulletLv < 3 && game.boss!=1) {
      this.get.play();
      myplane.bulletLv++;
    }
  },
  gethelp: function(myplane, help) {
    help.kill();
    if(this.helping == 0 && game.boss!=1){
      this.helping = 1;
      this.get.play();
      game.time.events.add(10000, function() {this.removehelp();}, this);
      this.helper1 = game.add.sprite(this.myplane.x-40, this.myplane.y, 'myplane');
      this.helper1.anchor.setTo(0.5, 0.5);
      this.helper1.scale.setTo(0.5, 0.5);
      this.helper1.animations.add('fly');
      this.helper1.animations.play('fly', 12, true);
      game.physics.arcade.enable(this.helper1);
      this.helper2 = game.add.sprite(this.myplane.x+40, this.myplane.y, 'myplane');
      this.helper2.anchor.setTo(0.5, 0.5);
      this.helper2.scale.setTo(0.5, 0.5);
      this.helper2.animations.add('fly');
      this.helper2.animations.play('fly', 12, true);
      game.physics.arcade.enable(this.helper2);
    }
  },
  removehelp: function(myplane, help) {
    this.helper1.kill();
    this.helper2.kill();
    this.helping = 0;
  },
  updateText: function() {
    this.scoreLabel.setText("Score: " + game.global.score);
  },
  updatelevel: function() {
    if(game.global.level < 3){
      this.enemyTeam.enemy1.velocity *= 2;
      this.enemyTeam.enemy2.velocity *= 2;
      this.enemyTeam.enemy3.velocity *= 2;
      this.enemyTeam.enemy1.bulletVelocity *= 1.5;
      this.enemyTeam.enemy2.bulletVelocity *= 1.5;
      this.enemyTeam.enemy3.bulletVelocity *= 1.5;
      this.enemyTeam.enemy1.bulletTimeInterval /= 1.5;
      this.enemyTeam.enemy2.bulletTimeInterval /= 1.5;
      this.enemyTeam.enemy3.bulletTimeInterval /= 1.5;
      game.global.level++;
    }
    this.levelLabel.setText(game.global.level+"/3");
  },
  bossfight: function() {
    game.boss = 1;  
    this.bosshp = 100;
    this.conshoot = 0;
    game.time.events.loop(1500, this.generateAlien, this);
    var tween = game.add.tween(this.boss);
    tween.to({y: 60}, 1459);
    tween.start();
    tween.onComplete.add(function() {
      this.myplane.bulletLv = 1;
      this.conshoot = 1;
      this.bosshp = 100;
      this.hpLabel.y = 13;
      this.bg.autoScroll(0,0);}, this);
  },
  generateAlien: function() {
    if(this.myplane.alive && game.time.now > this.alienTime && this.gena) {
      game.add.tween(this.boss).to({y: this.boss.y+30}, 1200).to({y: this.boss.y+5}, 300).start();
      game.add.tween(this.hpLabel).to({y: this.hpLabel.y+30}, 1200).to({y: this.hpLabel.y+5}, 300).start();
      var alien;
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(120, this.boss.y+45);
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(180, this.boss.y+45);
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(240, this.boss.y+45);
        alien.body.velocity.y = 50;
      }
      this.alienTime = game.time.now + 200;
    }
  },
  onSkillClick: function() {
    if(this.skillnum > 0){
      this.skillnum --;
      game.crash.play();
      this.enemy1.hitall();
      this.enemy2.hitall();
      this.enemy3.hitall();
    }
    this.skillbutton.setText("Skill: " + this.skillnum);
  },
  onPauseClick: function() {
    game.paused = true;
    var pausemenu = document.getElementById("pausemenu");
    pausemenu.style.display = "block";  
  },
  dead: function() {
    game.bgm.stop();
    if(this.helping){
      this.helper1.kill();
      this.helper2.kill();
    }
    var myexplode = game.add.sprite(this.myplane.x-25, this.myplane.y-25, 'myexplode');
    var anim = myexplode.animations.add('myexplode');
    myexplode.animations.play('myexplode', 10, false, true);
    anim.onComplete.add(this.lose, this);
  },
  lose: function() {
    this.deads.play();
    game.time.events.add(2000, function() {game.state.start('lose');}, this);
  },
  win: function() {
    game.bgm.stop();
    this.wins.play();
    game.time.events.add(3000, function() {game.state.start('win');}, this);
  },
}; 