var play2State = { 
  create: function() {
    game.global.score = 0;
    game.global.level = 1;
    game.boss = 0;
    this.conshoot = 1;
    this.gena = 1;

    this.bg = game.add.tileSprite(0, 0, game.width, game.height, 'background');
    this.bg.autoScroll(0, 20);

    this.plane2 = game.add.sprite(game.width/2, game.height/2-50, 'plane2');
    this.plane1 = game.add.sprite(game.width/2, game.height/2-50, 'plane1');

    this.plane1.anchor.setTo(0.5, 0.5);
    this.plane1.scale.setTo(2, 2);
    this.plane1.animations.add('fly');
    this.plane1.animations.play('fly', 12, true);
    game.physics.arcade.enable(this.plane1);
    this.plane1.body.collideWorldBounds = true;
    this.plane1.bulletLv = 2;
    this.plane1.health = 5;
    
    this.plane2.anchor.setTo(0.5, 0.5);
    this.plane2.scale.setTo(2, 2);
    this.plane2.animations.add('fly');
    this.plane2.animations.play('fly', 12, true);
    game.physics.arcade.enable(this.plane2);
    this.plane2.body.collideWorldBounds = true;
    this.plane2.bulletLv = 2;
    this.plane2.health = 5;
    
    game.add.tween(this.plane1).to({x: game.width/3, y: game.height - 40}, 1000, Phaser.Easing.Sinusoidal.InOut, true);
    game.add.tween(this.plane1.scale).to({x: 1, y: 1}, 1000, Phaser.Easing.Sinusoidal.InOut, true);
    game.add.tween(this.plane2).to({x: game.width*2/3, y: game.height - 40}, 1000, Phaser.Easing.Sinusoidal.InOut, true);
    game.add.tween(this.plane2.scale).to({x: 1, y: 1}, 1000, Phaser.Easing.Sinusoidal.InOut, true);

    var voicem = document.getElementById("music").value/100;
    var voices = document.getElementById("sound").value/100;
    game.bgm = game.add.audio('bgm', voicem, true);
    game.bgm.play();
    this.shoot = game.add.audio('shoot', voices, false);
    game.crash = game.add.audio('crash', voices, false);
    this.get = game.add.audio('get', voices, false);
    this.deads = game.add.audio('dead', voices, false);
    this.wins = game.add.audio('win', voices, false);
    
    this.bullets1 = game.add.group();
    this.bullets1.enableBody = true;
    this.bullets1.createMultiple(50, 'mybullet');
    this.bullets1.setAll('outOfBoundsKill', true);
    this.bullets1.setAll('checkWorldBounds', true);
    this.bullet1Time = 0;
    this.bullets2 = game.add.group();
    this.bullets2.enableBody = true;
    this.bullets2.createMultiple(50, 'mybullet');
    this.bullets2.setAll('outOfBoundsKill', true);
    this.bullets2.setAll('checkWorldBounds', true);
    this.bullet2Time = 0;

    this.emitters = game.add.emitter(0, 0, 150); 
    this.emitters.makeParticles('pixel'); 
    this.emitters.setYSpeed(-50, 50); 
    this.emitters.setXSpeed(-50, 50); 
    this.emitters.setScale(2, 0, 2, 0, 800); 
    this.emitters.gravity = 0;
    this.myemitter = game.add.emitter(0, 0, 150); 
    this.myemitter.makeParticles('mypixel'); 
    this.myemitter.setYSpeed(-50, 50); 
    this.myemitter.setXSpeed(-50, 50); 
    this.myemitter.setScale(2, 0, 2, 0, 800); 
    this.myemitter.gravity = 0;

    this.skillnum = 3;
    this.cursor = game.input.keyboard.createCursorKeys();
    this.wasd = { up: game.input.keyboard.addKey(Phaser.Keyboard.W), 
      left: game.input.keyboard.addKey(Phaser.Keyboard.A), 
      down: game.input.keyboard.addKey(Phaser.Keyboard.S),
      right: game.input.keyboard.addKey(Phaser.Keyboard.D) };
    
    this.bulups = game.add.group();
    this.bulups.enableBody = true;
    this.bulups.createMultiple(1, 'bulup');
    this.bulups.setAll('outOfBoundsKill', true);
    this.bulups.setAll('checkWorldBounds', true);
    this.bulupMaxWidth = game.width - game.cache.getImage('bulup').width;

    this.aliens = game.add.group();
    this.aliens.enableBody = true;
    this.aliens.createMultiple(50, 'alien');
    this.aliens.setAll('outOfBoundsKill', true);
    this.aliens.setAll('checkWorldBounds', true);
    this.aliens.forEach(function(alien) {
      alien.anchor.setTo(0.5, 0.5);
    }, this);
    this.alienTime = 0;
    
    game.time.events.loop(17000, this.generatebulup, this);
    game.time.events.loop(60000, this.updatelevel, this);
    game.time.events.add(180000, this.bossfight, this);
    
    this.enemyTeam = {
      enemy1: {
        game: this,
        selfPic: 'enemy1',
        bulletPic: 'bullet',
        explodePic: 'explode1',
        life: 3,
        velocity: 50,
        bulletVelocity: 200,
        selfTimeInterval: 1500,
        bulletTimeInterval: 1000,
        score: 10
      },
      enemy2: {
        game: this,
        selfPic: 'enemy2',
        bulletPic: 'bullet',
        explodePic: 'explode2',
        life: 8,
        velocity: 40,
        bulletVelocity: 250,
        selfTimeInterval: 2500,
        bulletTimeInterval: 1200,
        score: 20
      },
      enemy3: {
        game: this,
        selfPic: 'enemy3',
        bulletPic: 'bullet',
        explodePic: 'explode3',
        life: 15,
        velocity: 30,
        bulletVelocity: 300,
        selfTimeInterval: 6000,
        bulletTimeInterval: 1500,
        score: 50
      }
    }
    this.enemy1 = new Enemy(this.enemyTeam.enemy1);
    this.enemy1.init();
    this.enemy2 = new Enemy(this.enemyTeam.enemy2);
    this.enemy2.init();
    this.enemy3 = new Enemy(this.enemyTeam.enemy3);
    this.enemy3.init();

    this.boss = game.add.sprite(180, -70, 'boss');
    this.boss.anchor.setTo(0.5, 0.5);
    game.physics.arcade.enable(this.boss);

    this.lifeLabel = game.add.text(200, 10,'Health: 5/5', { font: '25px Arial', fill: '#ffffff' }); 
    this.levelLabel = game.add.text(310, 470,'1/3', { font: '25px Arial', fill: '#ffffff' }); 
    this.scoreLabel = game.add.text(200, 40, 'Score: 0', { font: '25px Arial', fill: '#ffffff' }); 
    this.hpLabel = game.add.text(180, -25, '100', { font: '25px Arial', fill: '#ff0000' });
    this.hpLabel.anchor.setTo(0.5, 0); 
    this.skillbutton = game.add.text(10, 470, 'Skill: 3', { font: '25px Arial', fill: '#ffffff' });
    this.skillbutton.inputEnabled = true;
    this.skillbutton.events.onInputDown.add(this.onSkillClick, this);
    this.pausebutton = game.add.button(10, 10, 'pause', this.onPauseClick, this);
  }, 
  update: function() {
    this.movePlayer1();
    this.movePlayer2();
    this.fireBullet();
    this.enemy1.enemyFire();
    if(game.global.level < 3) this.enemy2.enemyFire();
    this.enemy3.enemyFire();
    if(game.boss){
      this.moveAliens();
      game.physics.arcade.overlap(this.aliens, this.plane1, this.hitMyplane, null, this);
      game.physics.arcade.overlap(this.boss, this.plane1, this.crashMyplane, null, this);
      game.physics.arcade.overlap(this.aliens, this.plane2, this.hitMyplane, null, this);
      game.physics.arcade.overlap(this.boss, this.plane2, this.crashMyplane, null, this);
      game.physics.arcade.overlap(this.bullets1, this.aliens, this.hitAlien, null, this);
      game.physics.arcade.overlap(this.bullets1, this.boss, this.hitBoss, null, this);
      game.physics.arcade.overlap(this.bullets2, this.aliens, this.hitAlien, null, this);
      game.physics.arcade.overlap(this.bullets2, this.boss, this.hitBoss, null, this);
    }
    game.physics.arcade.overlap(this.bullets1, this.enemy1.enemys, this.enemy1.hitEnemy, null, this.enemy1);
    game.physics.arcade.overlap(this.bullets1, this.enemy2.enemys, this.enemy2.hitEnemy, null, this.enemy2);
    game.physics.arcade.overlap(this.bullets1, this.enemy3.enemys, this.enemy3.hitEnemy, null, this.enemy3);
    game.physics.arcade.overlap(this.bullets2, this.enemy1.enemys, this.enemy1.hitEnemy, null, this.enemy1);
    game.physics.arcade.overlap(this.bullets2, this.enemy2.enemys, this.enemy2.hitEnemy, null, this.enemy2);
    game.physics.arcade.overlap(this.bullets2, this.enemy3.enemys, this.enemy3.hitEnemy, null, this.enemy3);
    game.physics.arcade.overlap(this.enemy1.enemyBullets, this.plane1, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemyBullets, this.plane1, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemyBullets, this.plane1, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy1.enemyBullets, this.plane2, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemyBullets, this.plane2, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemyBullets, this.plane2, this.hitMyplane, null, this);
    game.physics.arcade.overlap(this.enemy1.enemys, this.plane1, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemys, this.plane1, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemys, this.plane1, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy1.enemys, this.plane2, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy2.enemys, this.plane2, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.enemy3.enemys, this.plane2, this.crashMyplane, null, this);
    game.physics.arcade.overlap(this.bulups, this.plane1, this.getbulup, null, this);
    game.physics.arcade.overlap(this.bulups, this.plane2, this.getbulup, null, this);
  },
  movePlayer1: function() {
    if (this.wasd.left.isDown) { 
      this.plane1.body.velocity.x = -200;
    }else if (this.wasd.right.isDown) { 
      this.plane1.body.velocity.x = 200;
    }else {
      this.plane1.body.velocity.x = 0; 
    }    

    if (this.wasd.up.isDown) { 
      this.plane1.body.velocity.y = -200;
    }else if (this.wasd.down.isDown) { 
      this.plane1.body.velocity.y = 200;
    }else {
      this.plane1.body.velocity.y = 0; 
    }      
  },
  movePlayer2: function() {
    if (this.cursor.left.isDown) { 
      this.plane2.body.velocity.x = -200;
    }else if (this.cursor.right.isDown) { 
      this.plane2.body.velocity.x = 200;
    }else {
      this.plane2.body.velocity.x = 0; 
    }    

    if (this.cursor.up.isDown) { 
      this.plane2.body.velocity.y = -200;
    }else if (this.cursor.down.isDown) { 
      this.plane2.body.velocity.y = 200;
    }else {
      this.plane2.body.velocity.y = 0; 
    }      
  },
  moveAliens: function() {
    var left,right;
    if(this.plane1.alive && this.plane2.alive){
      if(this.plane1.x<this.plane2.x){
        left = this.plane1.x;
        right = this.plane2.x;
      }else{
        left = this.plane2.x;
        right = this.plane1.x;
      }
    }else if(this.plane1.alive){
      left = this.plane1.x;
      right = this.plane1.x;
    }else if(this.plane2.alive){
      left = this.plane2.x;
      right = this.plane2.x;
    } 
    this.aliens.forEachExists(function(alien) {
      if(alien.x > right) {
        alien.body.velocity.x =  -20;
      }else if(alien.x < left) {
        alien.body.velocity.x = 20;
      }
    }, this);
  },
  generatebulup: function() {
    var bulup = this.bulups.getFirstExists(false);
    if(bulup && game.boss!=1) {
      bulup.reset(game.rnd.integerInRange(0, this.bulupMaxWidth), -game.cache.getImage('bulup').height);
      bulup.body.velocity.y = 100;
    }
  },
  fireBullet: function() {
    if(this.plane1.alive && game.time.now > this.bullet1Time && this.conshoot) {
      try {
        this.shoot.play();
      } catch(e) {}
      var bullet;
      bullet = this.bullets1.getFirstExists(false);
      if(bullet) {
        bullet.reset(this.plane1.x -4, this.plane1.y -45);
        bullet.body.velocity.y = -400;
        this.bullet1Time = game.time.now + 200;
      }
      if(this.plane1.bulletLv >= 2) {
        bullet = this.bullets1.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane1.x -4, this.plane1.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -40;
          this.bullet1Time = game.time.now + 200;
        }
        bullet = this.bullets1.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane1.x -4, this.plane1.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 40;
          this.bullet1Time = game.time.now + 200;
        }
      }
      if(this.plane1.bulletLv >= 3) {
        bullet = this.bullets1.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane1.x -4, this.plane1.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -80;
          this.bullet1Time = game.time.now + 200;
        }
        bullet = this.bullets1.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane1.x -4, this.plane1.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 80;
          this.bullet1Time = game.time.now + 200;
        }
      }
    }
    if(this.plane2.alive && game.time.now > this.bullet2Time && this.conshoot) {
      this.shoot.play();
      var bullet;
      bullet = this.bullets2.getFirstExists(false);
      if(bullet) {
        bullet.reset(this.plane2.x -4, this.plane2.y -45);
        bullet.body.velocity.y = -400;
        this.bullet2Time = game.time.now + 200;
      }
      if(this.plane2.bulletLv >= 2) {
        bullet = this.bullets2.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane2.x -4, this.plane2.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -40;
          this.bullet2Time = game.time.now + 200;
        }
        bullet = this.bullets2.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane2.x -4, this.plane2.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 40;
          this.bullet2Time = game.time.now + 200;
        }
      }
      if(this.plane2.bulletLv >= 3) {
        bullet = this.bullets2.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane2.x -4, this.plane2.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = -80;
          this.bullet2Time = game.time.now + 200;
        }
        bullet = this.bullets2.getFirstExists(false);
        if(bullet) {
          bullet.reset(this.plane1.x -4, this.plane2.y -45);
          bullet.body.velocity.y = -400;
          bullet.body.velocity.x = 80;
          this.bullet2Time = game.time.now + 200;
        }
      }
    }
    this.bulletTime = game.time.now + 200;
  },
  hitMyplane: function(myplane, bullet) {
    bullet.kill();
    this.myemitter.x = bullet.x+4; 
    this.myemitter.y = bullet.y+10; 
    this.myemitter.start(true, 800, null, 15);
    myplane.health --;
    this.lifeLabel.setText('Health: ' + this.plane1.health +"/"+ this.plane2.health);
    if(myplane.health > 0) {
      if(myplane.bulletLv > 1) myplane.bulletLv--;
    } else {
      myplane.kill();
      this.dead(myplane.x,myplane.y);
    }
  },
  hitAlien: function(alien, bullet) {
    bullet.kill();
    this.emitters.x = bullet.x+4; 
    this.emitters.y = bullet.y+10; 
    this.emitters.start(true, 800, null, 15);
    alien.kill();
    game.global.score += 5;
    this.updateText();
  },
  hitBoss: function(boss, bullet) {
    bullet.kill();
    if(this.bosshp > 1) {
      this.bosshp --;
      this.hpLabel.setText(this.bosshp);
      this.emitters.x = bullet.x+4; 
      this.emitters.y = bullet.y+10; 
      this.emitters.start(true, 800, null, 15);
    } else {
      this.conshoot = 0;
      this.hpLabel.setText(0);
      if(this.gena){
        this.explode = game.add.sprite(this.boss.x, this.boss.y, 'explode3');
        this.explode.anchor.setTo(0.5, 0.5);
        this.explode.scale.setTo(3, 3);
        this.explode.animations.add('explode3');
        this.explode.animations.play('explode3', 10, false);
      }
      this.aliens.forEachExists(function(alien) {
        alien.kill();
      }, this);
      this.gena = 0;
      game.global.score += 5000;
      game.crash.play();
      this.win();
    }
  },
  crashMyplane: function(myplane, enemy) {
    var x = myplane.x;
    var y = myplane.y;
    if(game.boss){
      if(this.plane1.alive && this.plane1.y-this.boss.y <= 90){
        this.plane1.health = 0;
        x = this.plane1.x;
        y = this.plane1.y;
        this.plane1.kill();
      }else if(this.plane2.alive && this.plane2.y-this.boss.y <= 90){
        this.plane2.health = 0;
        x = this.plane2.x;
        y = this.plane2.y;
        this.plane2.kill();
      }else{
        myplane.health = 0;
        myplane.kill();
      }
    }else{
      myplane.health = 0;
      myplane.kill();
    }
    this.lifeLabel.setText('Health: ' + this.plane1.health +"/"+ this.plane2.health);
    this.dead(x,y);
  },
  getbulup: function(myplane, bulup) {
    bulup.kill();
    this.get.play();
    if(myplane.bulletLv < 3  && game.boss!=1) {
      myplane.bulletLv++;
    }
  },
  updateText: function() {
    this.scoreLabel.setText("Score: " + game.global.score);
  },
  updatelevel: function() {
    if(game.global.level < 3){
      this.enemyTeam.enemy1.velocity *= 2;
      this.enemyTeam.enemy2.velocity *= 2;
      this.enemyTeam.enemy3.velocity *= 2;
      this.enemyTeam.enemy1.bulletVelocity *= 1.5;
      this.enemyTeam.enemy2.bulletVelocity *= 1.5;
      this.enemyTeam.enemy3.bulletVelocity *= 1.5;
      this.enemyTeam.enemy1.bulletTimeInterval /= 1.5;
      this.enemyTeam.enemy2.bulletTimeInterval /= 1.5;
      this.enemyTeam.enemy3.bulletTimeInterval /= 1.5;
      game.global.level++;
    }
    this.levelLabel.setText(game.global.level+"/3");
  },
  bossfight: function() {
    game.boss = 1;   
    this.bosshp = 200;
    this.conshoot = 0;
    game.time.events.loop(1500, this.generateAlien, this);
    var tween = game.add.tween(this.boss);
    tween.to({y: 60}, 1459);
    tween.start();
    tween.onComplete.add(function() {
      this.plane1.bulletLv = 1;
      this.plane2.bulletLv = 1;
      this.conshoot = 1;
      this.bosshp = 200;
      this.hpLabel.y = 13;
      this.bg.autoScroll(0,0);}, this);
  },
  generateAlien: function() {
    if(game.time.now > this.alienTime && this.gena) {
      game.add.tween(this.boss).to({y: this.boss.y+30}, 1200).to({y: this.boss.y+5}, 300).start();
      game.add.tween(this.hpLabel).to({y: this.hpLabel.y+30}, 1200).to({y: this.hpLabel.y+5}, 300).start();
      var alien;
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(60, this.boss.y+45);
        alien.body.velocity.x = -20;
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(120, this.boss.y+45);
        alien.body.velocity.x = -20;
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(180, this.boss.y+45);
        alien.body.velocity.x = 20;
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(240, this.boss.y+45);
        alien.body.velocity.x = 20;
        alien.body.velocity.y = 50;
      }
      alien = this.aliens.getFirstExists(false);
      if(alien) {
        alien.reset(300, this.boss.y+45);
        alien.body.velocity.x = 20;
        alien.body.velocity.y = 50;
      }
      this.alienTime = game.time.now + 200;
    }
  },
  onSkillClick: function() {
    if(this.skillnum > 0){
      this.skillnum --;
      game.crash.play();
      this.enemy1.hitall();
      this.enemy2.hitall();
      this.enemy3.hitall();
    }
    this.skillbutton.setText("Skill: " + this.skillnum);
  },
  onPauseClick: function() {
    game.paused = true;
    var pausemenu = document.getElementById("pausemenu");
    pausemenu.style.display = "block";
  },
  dead: function(x,y) {
    var myexplode = game.add.sprite(x-25, y-25, 'myexplode');
    var anim = myexplode.animations.add('myexplode');
    myexplode.animations.play('myexplode', 10, false, true);
    if(this.plane1.health == 0 && this.plane2.health == 0){
      anim.onComplete.add(this.lose, this);
    }
  },
  lose: function() {
    game.bgm.stop();
    this.deads.play();
    game.time.events.add(2000, function() {game.state.start('lose');}, this);
  },
  win: function() {
    game.bgm.stop();
    this.wins.play();
    game.time.events.add(3000, function() {game.state.start('win');}, this);
  },
}; 