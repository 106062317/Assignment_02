var winState = {
  create: function() {
    game.stage.backgroundColor = '#999999'; 
    var winLabel = game.add.text(game.width/2, game.height/2-50, 'You Win', { font: '25px Arial', fill: '#00ff00' }); 
    winLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen 
    var scoreLabel = game.add.text(game.width/2, game.height/2+50, 'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);

    var menup = document.getElementById("pausemenu");
    menup.style.display = "none";

    this.myplane = game.add.sprite(game.width/2, game.height/2, 'myplane');
    this.myplane.anchor.setTo(0.5, 0.5);
    this.myplane.animations.add('fly');
    this.myplane.animations.play('fly', 12, true);

    var re = document.getElementById("restartmenu");
    var m0 = document.getElementById("1p");
    var m1 = document.getElementById("2p");
    var db = firebase.database();
    var n1,n2,n3,n4,n5;
    var s1,s2,s3,s4,s5;
    if(m0.selected){
      db.ref("1p").once('value').then(function (snapshot) {
        var data = snapshot.val();
        n5 = data.rank5.name;
        s5 = data.rank5.score;
        if(s5 < game.global.score){
          n1 = data.rank1.name;
          s1 = data.rank1.score;    
          n2 = data.rank2.name;
          s2 = data.rank2.score;
          n3 = data.rank3.name;
          s3 = data.rank3.score;
          n4 = data.rank4.name;
          s4 = data.rank4.score;
          n5 = game.global.name;
          s5 = game.global.score;
          var rank = [{name:n1, score:s1},{name:n2, score:s2},{name:n3, score:s3},{name:n4, score:s4},{name:n5, score:s5}];
          rank.sort(function(a, b){return b.score - a.score});
          db.ref("/1p/rank1").set(rank[0]);
          db.ref("/1p/rank2").set(rank[1]);
          db.ref("/1p/rank3").set(rank[2]);
          db.ref("/1p/rank4").set(rank[3]);
          db.ref("/1p/rank5").set(rank[4]);
          document.getElementById('r11').innerHTML = "1." + rank[0].name + ":" + rank[0].score;
          document.getElementById('r21').innerHTML = "2." + rank[1].name + ":" + rank[1].score;
          document.getElementById('r31').innerHTML = "3." + rank[2].name + ":" + rank[2].score;
          document.getElementById('r41').innerHTML = "4." + rank[3].name + ":" + rank[3].score;
          document.getElementById('r51').innerHTML = "5." + rank[4].name + ":" + rank[4].score;
        }
        re.style.display = "block";
      });  
    }else if(m1.selected){
      db.ref("2p").once('value').then(function (snapshot) {
        var data = snapshot.val();
        n5 = data.rank5.name;
        s5 = data.rank5.score;
        if(s5 < game.global.score){
          n1 = data.rank1.name;
          s1 = data.rank1.score;    
          n2 = data.rank2.name;
          s2 = data.rank2.score;
          n3 = data.rank3.name;
          s3 = data.rank3.score;
          n4 = data.rank4.name;
          s4 = data.rank4.score;
          n5 = game.global.name;
          s5 = game.global.score;
          var rank = [{name:n1, score:s1},{name:n2, score:s2},{name:n3, score:s3},{name:n4, score:s4},{name:n5, score:s5}];
          rank.sort(function(a, b){return b.score - a.score});
          db.ref("/2p/rank1").set(rank[0]);
          db.ref("/2p/rank2").set(rank[1]);
          db.ref("/2p/rank3").set(rank[2]);
          db.ref("/2p/rank4").set(rank[3]);
          db.ref("/2p/rank5").set(rank[4]);
          document.getElementById('r12').innerHTML = "1." + rank[0].name + ":" + rank[0].score;
          document.getElementById('r22').innerHTML = "2." + rank[1].name + ":" + rank[1].score;
          document.getElementById('r32').innerHTML = "3." + rank[2].name + ":" + rank[2].score;
          document.getElementById('r42').innerHTML = "4." + rank[3].name + ":" + rank[3].score;
          document.getElementById('r52').innerHTML = "5." + rank[4].name + ":" + rank[4].score;
        }
        re.style.display = "block";
      });  
    }
  }
}; 
